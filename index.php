<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <link rel="stylesheet" href="style.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="index.js"></script>
    <style>
        body {
            background-color: rgb(213, 225, 239);
            transition: background-color 0.5s ease;
        }
    </style>
</head>

<body>
    <div id="google_translate_element">
        Change Language
    </div>
    <div class="top-header">
        <a href="mainpage.php"><h1>www.medgeo.net</h1></a>
        <p>ქართული სამედიცინო ინტერნეტ-ქსელი, 1996 წლიდან</p>
        <?php
            include "validations2.php";
            if(isset($_SESSION['loginName'])) {
                $loginname = $_SESSION['loginName'];
                echo "<h2 class='a-1'>$loginname</h2>";
                echo "<form method='post'>
                        <input class='a-1' type='submit' name='logout' value='logout'>            
                    </form>";
            } else {
        ?>
        <a class="a-1" href="sign in.php">შესვლა</a>
        <a class="a-1" href="registration.php">რეგისტრაცია</a>
        <?php
            }
        ?>
        <button onclick="toggleBackgroundColor()" id="background-btn">CHANGE THEME</button>
        <nav id="nav">
            <input type="checkbox" id="btn-area" class="menu-icon">
            <label for="btn-area" onclick="Logo"></label>
            <ul>
                <li><a href="">ჩვენ</a>
                    <ul>
                        <li><a href="contact.php">კონტაქტი</a></li>
                        <li><a href="">მედიცინა სოც. ქსელებში</a></li>
                    </ul>
                </li>
                <li><a href="">დაავადებები</a></li>
                <li><a href="">დიაგნოსტიკა</a></li>
                <li><a href="">პრეპარატები</a>
                    <ul>
                        <li><a href="">სამკურნალო მცენარეები</a></li>
                    </ul>
                </li>
                <li><a href="">სოც.ქსელები</a></li>
                <li><a href="">სტუდენტი</a>
                    <ul>
                        <li><a href="">ლექსიკონები</a></li>
                        <li><a href="">ატლასები</a></li>
                        <li><a href="">სამედიც. ლიტერატურა</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    <div class="mainheader">
        <img src="medgeoupdated.jpg" alt="">
    </div>
    <div class="animation"><a>ყველაფერი მედიცინის შესახებ</a></div>
    <div class="medgeo">
        <h2>medgeo.net</h2>
        <a>საიტი Medgeo.net 1996 წელს შეიქმნა USAID -ის რესურსებით, ლალი დათეშიძის პროექტით. იგი მას შემდეგ <br>
            <span>სტაბილურად ვითარდება</span> და დღეს საიტების დიდ ქსელს წარმოადგენს.</a>
        <button class="medgeobutton">გრანტები და მადლობები</button>
    </div>
    <div class="container mt-5">
        <div class="row" style="height: 400px;">
            <div class="col-sm-4">
                <h3>ჩვენი საიტები</h3>
                <p>Medgeo.net 1996 წელს შეიქმნა, როგორც მედიცინის საიტი. დღეს იგი მრავალ საიტსა და სოც. ქსელებს
                    აერთიანებს</p>
                <button>Read More >></button>

            </div>
            <div class="col-sm-4">
                <h3>ჩვენი საკონსულტაციო ქსელი</h3>
                <p>Medgeo.net დიდი საკონსულტაციო ქსელიცაა, რომელიც მრავალ სხვადასხვა მიმართულებას აერთიანებს</p>
                <button>Read More >></button>

            </div>
            <div class="col-sm-4">
                <h3>მკურნალობა საზღვარგარეთ</h3>
                <p>კონსულტაციები, დიაგნოსტიკა, მკურნალობა საზღვარგარეთ, წამყვან სამედიცინო კლინიკებში</p>
                <button>Read More >></button>
            </div>
            <aside class="search">	<form method="get" class="searchform" action="http://127.0.0.1:5500/mainpage.html" role="search">
                <input type="text" class="search" name="s" value="" placeholder="ძებნა">
                <input type="submit" class="searchsubmit" name="submit" value="ძებნა">
            </form></aside>
            <div class="right-menu">
                <ul>
                    <h4>კურორტები, აქტიური დასვენება</h4>
                    <hr>
                        <?php
                            include "kurortebi.php";
                        ?>
                    <hr>
                </ul>
            </div>
            <div class="right-menu-2">
                <ul>
                    <h4>ქალბატონები</h4>
                    <hr>
                    <?php
                        include "kalbatonebi.php";
                    ?>
                </ul>
            </div>
            <div class="right-menu-3">
                <ul>
                    <h4>შვილები</h4>
                    <hr>
                    <?php
                        include "shvilebi.php";
                    ?>
                    <hr>
                </ul>
            </div>
            <div class="right-menu-3">
                <ul>
                    <h4>ავადმყოფის მოვლა</h4>
                    <hr>
                    <?php
                        include "movla.php";
                    ?>
                    <hr>
                </ul>
            </div>
            <div class="right-menu-4">
                <ul>
                    <h4>ალტერნატიული მკურნალობა</h4>
                    <hr>
                    <?php
                        include "mkurnaloba.php";
                    ?>
                    <hr>
                </ul>
            </div>
            <div class="right-menu-5">
                <ul>
                    <h4>ფსიქოლოგია</h4>
                    <hr>
                    <?php
                        include "psiqologia.php";
                    ?>
                    <hr>
                </ul>
            </div>
            <div class="right-menu-6">
                <ul>
                    <h4>თემატური საიტები</h4>
                    <hr>
                    <?php
                        include "tematuri.php";
                    ?>
                    <hr>
                </ul>
            </div>
            <div class="article-1">
                <a href=""><img src="udziloba.png" alt=""></a>
                <a href=""><h4 class="h4-11">უძილობა</h4></a>
                <p class="udziloba">უძილობა – ეს არის ძილის დარღვევა; ხასიათდება არა მარტო არასაკმარისი ძილით, არამედ კარგად გამოძინების უუნარობით, ასევე მოიცავს ჩაძინების პრობლემებს, ძილის არასაკმარის ხანგრძლივობას ან ადრე გამოღიძებას, რომლის შემდეგ რთულია ხელახლა ჩაძინება. უძილობის მიზეზები არაკომფორტული გარემო, რომელშიც უნდა დაიძინოს ადამიანმა, მაგ: ხმაური, სიცივე ან სიცხე, ძალიან მაგარი ან რბილი საწოლი. ცხოვრების წესისი შეცვლა, მაგ: მოგზაურობა, ახალ საცხოვრებელში.</p>
                <button class="btn-11">Read More</button>
            </div>
            <div class="article-1">
                <a href=""><img src="ომეგა-3-ის-თვისებები-570x342.jpg" alt=""></a>
                <a href=""><h4 class="h4-1">ომეგა-3 -ის თვისებები</h4></a>
                <p class="p-12">ომეგა-3 -ის თვისებები მრავალი მიზეზის გამო, ბევრი ამჯობინებს ომეგა-3 (omega-3) უშუალოდ მწარმოებლისაგან გამოიწეროს, ყველაზე სანდო ინტერნეტ-მაღაზიიდან „ამაზონი“. თუ გსურთ ომეგა-3 (omega-3) -ის გამოწერა ინტერნეტ-მაღაზია ,,ამაზონიდან”  დააკლიკეთ წინამდებარე  ჟოლოსფერ ტექსტს. წინამდებარე, ჟოლოსფერ ტექსტზე დაკლიკებით, მოხვდებით ამაზონის ომეგა-3 (omega-3) -ის ქვეგანყოფილებაში, სადაც წარმოდგენილია საუკეთესო მწარმოებლების, სანდო პროდუქცია ♥ ომეგა-3 -ის გამოწერა სელის თესლი ცხიმოვანი  მჟავების –  ომეგა-3,</p>
                <button class="btn-2">Read More</button>
            </div> 
            <div class="article-1">
                <a href=""><img src="ომეგა-3-2.png" alt=""></a>
                <a href=""><h4 class="h4-2">რა არის ომეგა-3</h4></a>
                <p class="p-13">რა არის ომეგა-3 მრავალი მიზეზის გამო, ბევრი ამჯობინებს ომეგა-3 (omega-3) უშუალოდ მწარმოებლისაგან გამოიწეროს, ყველაზე სანდო ინტერნეტ-მაღაზიიდან „ამაზონი“. თუ გსურთ ომეგა-3 (omega-3) -ის გამოწერა ინტერნეტ-მაღაზია ,,ამაზონიდან”  დააკლიკეთ წინამდებარე  ჟოლოსფერ ტექსტს. წინამდებარე, ჟოლოსფერ ტექსტზე დაკლიკებით, მოხვდებით ამაზონის ომეგა-3 (omega-3) -ის ქვეგანყოფილებაში, სადაც წარმოდგენილია საუკეთესო მწარმოებლების, სანდო პროდუქცია ♥ ომეგა-3 -ის გამოწერა სელის თესლი ცხიმოვანი  მჟავების –  ომეგა-3,</p>
                <button class="btn-4">Read More</button>
            </div> 
            <div class="article-1">
                <a href=""><h4 class="h4-3">რისგან შედგება კბილის იმპლანტი?</h4></a>
                <p class="implant">იმპლანტი არის ხელოვნური ფესვი, ბიოშეთავსებადი და ძალიან მტკიცე მასალისგან დამზადებული. რა არის იმპლანტი? თითოეული კბილი შედგება გვირგვინისა და ფესვისგან. გვირგვინი კბილის ხილული ნაწილია, რომელიც საჭიროა საკვების დასაღეჭად. ფესვი ღრძილის ქვეშ მდებარეობს და ამაგრებს კბილს ყბის ძვალში. ხელოვნურ კბილსაც აქვს ფესვი, რომელსაც იმპლანტი ეწოდება. თუ რაიმე მიზეზის გამო თქვენ დაკარგეთ კბილი, იმპლანტი ამჟამად ყველაზე საიმედო და</p>
                <button class="btn-14">Read More</button>
            </div> 
            <div class="article-1">
                <a href=""><h4 class="h4-4">ძირითადი პრობლემები სიბრძნის კბილების გამო</h4></a>
                <p class="sibrdzniskbili" >სიბრძნის კბილები დახასიათება სიბრძნის კბილები რიგით მესამე მოლარებია, რომლებიც 18-25 წლის ასაკში ამიჭრება. სიბრძნის კბილებს ასე უწოდებენ იმიტომ, რომ ისინი სხვა კბილებთან შედარებით მნიშვნელოვნად გვიან ამოდიან, იმ დროს, როდესაც ადამიანი ბავშვობის ასაკიდან მოწიფულობის ასაკში გადადის. გერმანელმა დოქტორმა ფოლმა ჩაატარა კვლევა და დაასკვნა, რომ ადამიანის ორგანიზმში ყოველი კბილი დაკავშირებულია შინაგან ორგანოებთან ან რომელიმე სისტემასთან. მისი დასკვნით, სიბრძნის კბილები</p>
                <button class="btn-5">Read More</button>
            </div> 
            <div class="article-1">
                <a href=""><img class="dagliloba" src="ქდს-570x342.png" alt=""></a>
                <a href=""><h4 class="h4-5">ქრონიკული დაღლილობის სინდრომი</h4></a>
                <p class="sindromi">ქრონიკული დაღლილობის სინდრომი, კოგნიტიურ ქცევითი (ბიჰეივიორული) თერაპია ავტორი: დოქტორი დავით მალიძე. საავტორო უფლებები დაცულია. სტატიის გადასაბეჭდად მიმართეთ  სტატიაში მოცემულია პრაქტიკულად უცნობი ეტიოლოგიისა და პათოგენეზის დაავადების დიაგნოზის, დიფერენციალური დიაგნოზის, პროგნოზისა და მკურნალობის საკითხები. ქრონიკული დაღლილობის სინდრომი ყოველწლიურად სულ უფრო მასობრივ ხასიათს იღებს და  არა მხოლოდ ზრდასრულებში, არამედ მოზარდებსა და ბავშვებშიც შრომისუნარიანობის დაქვეითების, ცხოვრების ნორმალური რიტმის დარღვევის</p>
                <button class="btn-6">Read More</button>
            </div> 
            <div class="article-1">
                <a href=""><img src="ღამის-ოფლიანობა-ბავშვებში.png" alt=""></a>
                <a href=""><h4 class="h4-6">ღამის ოფლიანობა ბავშვებში</h4></a>
                <p>ღამის ოფლიანობა (ჰიპერჰიდროზი)  – მდგომარეობა, როდესაც აღინიშნება ღამის განმავლობაში ჭარბი ოფლიანობა. ბავშვებში ეს ხშირად ჩვეული მოვლენაა და არ საჭიროებს ჩარევას. მიზეზი უმწიფარი თერმორეგულაციური  სისტემა და ჯერ კიდევ ჩამოუყალიბებელი საოფლე ჯირკვლებია, თუმცა არ უნდა გამოგვეპაროს ოფლიანობის  გამომწვევი ისეთი პათოლოგიები, რომლებიც სათანადო მკურნალობას საჭიროებს.   ღამის ოფლიანობის მიზეზები ძილის დროს გადახურება – რაც განპირობებულია ოთახში მაღალი ტემპერატურით</p>
                <button class="btn-4">Read More</button>
            </div> 
            <div class="article-1">
                <a href=""><img src="trofikuli-wyluli.png" alt=""></a>
                <a href=""><h4 class="h4-7">ტროფიკული წყლული</h4></a>
                <p>ტროფიკული წყლული (ბერძ. trophē საკვები, კვება) – კანის ან ლორწოვანი გარსის დეფექტი, რომელიც გამოწვეულია ნეკროზული კანის მოცილების შედეგად და გამოიხასიათდება ტორპიდული მომდინარეობით, შეხორცებისადმი მცირედი ტენდენციით და რეციდივირებისკენ მიდრეკილებით. ეტიოლოგია, პათოგენეზი ტროფიკული წყლული ვითარდება სისხლისა და ლიმფის მიმოქცევის დარღვევის შედეგად ან ინერვაციის მოშლის საპასუხოდ, რის შედეგადაც ქვეითდება კანის კვება. ტროფიკული წყლულის დროს გამოსაყენებელი საშუალებები ინტერნეტ-მაღაზია ,,ამაზონზე”</p>
                <button class="btn-4">Read More</button>
            </div> 
            <div class="article-1">
                <a href=""><img src="depresia.png" alt=""></a>
                <a href=""><h4 class="h4-8">დეპრესია</h4></a>
                <p class="depresia">დეპრესია (დეპრესიული სინდრომი) – ავადმყოფური მდგომარეობაა, რომელიც ვლინდება  ფიზიკური (ზოგადი ტონუსის დაქვეთება, მოძრაობების შენელება, საკვების მონელების შენელება) და ფსიქიკური (დათრგუნული გუნებ-განწყობა, შენელებული ფსიქიკური პროცესები) დარღვევებით. ეს სიმპტომები შესაძლებელია სხვადასხვანაირად გამოვლინდეს – დათრგუნული გუნებაგანწყობით – სევდიანობით, ინტერესების დაკარგვითა და ღრმა მწუხარებით; მძიმე შემთხვევებში – საკუთარი სიკვდილის სურვილით; ფსიქიკური პროცესების შენელება ვლინდება აზროვნების შენელებით, აზროვნების „გაყინვისა“  და</p>
                <button class="btn-4">Read More</button>
            </div> 
            <div class="article-1">
                <a href=""><img src="ყურადღების-დეფიციტის-სინდრომი.png" alt=""></a>
                <a href=""><h4 class="h4-9">ყურადღების დეფიციტის სინდრომი ბავშვებში</h4></a>
                <p class="yuradgeba">ყურადღების დეფიციტის სინდრომი ბავშვებში. მრავალი ფსიქიკური დარღვევა იღებს დასაბამს ჯერ კიდევ ბავშვობიდან. მათ შორის ყველაზე ხშირია ყურადღების დეფიციტის სინდრომი.  ასეთ ბავშვებს აქვთ ქცევითი დარღვევები, მაგრამ არ აღენიშნებათ თავის ტვინის  ორგანული ხასიათის დაზიანება. ყურადღების დეფიციტის სინდრომი ხასიათდება, პირველ რიგში, გადაჭარბებული მოძრაობითი აქტივობით, თავის ტვინის მინიმალური დარღვევებით, ბავშვთა  მსუბუქი ენცეფალოპათიით. ამ დარღვევას სხვაგვარად უწოდებენ თავის ტვინის მინიმალურ დისფუნქციას.</p>
                <button class="btn-4">Read More</button>
            </div> 
            <div class="article-1">
                <a href=""><img src="nevrastenia.png" alt=""></a>
                <a href=""><h4 class="h4-10">ნევრასთენია</h4></a>
                <p class="nevrastenia">ნევრასთენია – ნევროზი, რომელიც გამოწვეულია გადაღლით ან ფსიქოტრავმული ფაქტორების ხანგრძლივი ზემოქმედებით. ვლინდება მომატებული აგზნებადობით, ემოციური არამდგრადობით, სწრაფი გამოფიტვით, ძილის დარღვევით, ვეგეტატური დარღვევებით. როგორც დამოუკიდებელი დაავადება, ის 1869 წელს ამერიკელმა ფსიქოლოგმა გეორგ ბირდმა აღწერა. ნევრასთენიის ყველაზე ხშირი სიმპტომია თავის ტკივილი; იგი იწყება ძირითადად საღამოობით და აქვს დიფუზური ხასიათი. სხვა სიმპტომებია: თავბრუსხვევა სწრაფი დაღლა გონებრივი მუშაობისას ფიზიკური</p>
                <button class="btn-4">Read More</button>
            </div> 
            <div class="page-div" style="height: 50px;">
                <a href=""><div class="page">1</div></a>
                <a href=""><div class="page">2</div></a>
                <a href=""><div class="page">3</div></a>
                <a href=""><div class="page">...</div></a>
                <a href=""><div class="page">></div></a>
            </div>
            <div class="footer">
                <hr class="hr-footer">
                <p class="p-footer">We are a participant in the Amazon Services LLC Associates Program, an affiliate advertising program designed to provide a means for us to earn fees by linking to Amazon.com and affiliated sites
                </p>
                <p class="p-footer-2">Theme by <a href="">redline.</a> Powered by <a href="">redline.</a></p>
            </div>
        </div>

    </div>
    <div class="languages">
        <a href="#"title="Azerbaijani"><img src="//www.medgeo.net/wp-content/plugins/gtranslate/flags/24/az.png" height="24" width="24" alt="Azerbaijani"> <span>Azerbaijani</span></a> <a href="#" title="English"><img src="//www.medgeo.net/wp-content/plugins/gtranslate/flags/24/en.png" height="24" width="24" alt="English"> <span>English</span></a> <a href="#" title="Georgian"><img src="//www.medgeo.net/wp-content/plugins/gtranslate/flags/24/ka.png" height="24" width="24" alt="Georgian"> <span>Georgian</span></a> <a href="#"  title="Russian"><img src="//www.medgeo.net/wp-content/plugins/gtranslate/flags/24/ru.png" height="24" width="24" alt="Russian"> <span>Russian</span></a> <a href="#"  title="Turkish"><img src="//www.medgeo.net/wp-content/plugins/gtranslate/flags/24/tr.png" height="24" width="24" alt="Turkish"> <span>Turkish</span></a>
    </div>
</body>

</html>